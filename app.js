var express = require('express');
var app = express();
C = require("child_process");

app.get('/', function (req, res) {
    res.sendFile('~/volco/index.htm');
});

app.get('/up', function (req, res) {
	C.execSync("pactl -- set-sink-volume 0 +10%")
	C.execSync("amixer set Master 10%+")
    res.send('<html><body><h1>+Hello World</h1></body></html>');
});

app.get('/down', function (req, res) {
	C.execSync("pactl -- set-sink-volume 0 -3%")
	C.execSync("amixer set Master 3%-")
    res.send('<html><body><h1>-Hello World</h1></body></html>');
});

app.get('/zero', function (req, res) {
    C.execSync("pactl -- set-sink-volume 0 0%")
C.execSync("amixer set Master 0%")
    res.send('<html><body><h1>0Hello World</h1></body></html>');
});


var server = app.listen(1909, function () {
    console.log('Node server is running..');
});


